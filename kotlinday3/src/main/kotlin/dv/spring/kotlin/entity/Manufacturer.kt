package dv.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.transaction.Transactional


@Entity
data class Manufacturer(var name: String? = null,
                        var telNo: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany(mappedBy = "manufacturer")
    var products = mutableListOf<Product>()
//    lateinit var address: Address
}
