package dv.spring.kotlin.entity.dto

class ShoppingCartCustomerSelectedProductDto(
        var selectedProduct: SelectedProductDto? = null,
        var customer : String? = null
)