//package dv.spring.kotlin.controller
//
//import dv.spring.kotlin.entity.MyPerson
//import dv.spring.kotlin.entity.MyPersons
//import dv.spring.kotlin.entity.Person
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class HelloWorldController {
//    @GetMapping("/helloWorld")
//    fun getHelloWorld(): String {
//        return "Hello World"
//    }
//
//    @GetMapping("/test")
//    fun getTestCamt(): String {
//        return "CAMT"
//    }
//
//    @GetMapping("/person")
//    fun getPerson(): ResponseEntity<Any> {
//        val person = Person("Som", "Som", 15)
//        return ResponseEntity.ok(person)
//    }
//
//    @GetMapping("/myperson")
//    fun getMyPerson(): ResponseEntity<Any> {
//        val myperson = MyPerson("Chonnabot", "Duangpra", 15)
//        return ResponseEntity.ok(myperson)
//    }
//
//    @GetMapping("/persons")
//    fun getPersons(): ResponseEntity<Any> {
//        val person01 = Person("somchai", "somrak", 15)
//        val person02 = Person("Prayut", "Chan", 62)
//        val person03 = Person("Lung", "Pom", 65)
//        val persons = listOf<Person>(person01, person02, person03)
//        return ResponseEntity.ok(persons)
//    }
//
//    @GetMapping("/myPersons")
//    fun getMyPersons(): ResponseEntity<Any> {
//        val person01 = MyPersons("Osora", "Tsubasa", "Nachansu", 10)
//        val person02 = MyPersons("Hyuoka", "Kojiro", "Meiwa", 9)
//
//        val myPersons = listOf<MyPersons>(person01, person02)
//        return ResponseEntity.ok(myPersons)
//    }
//
//    @GetMapping("/params/{name}/{surname}/{age}")
//    fun getPathParam( @PathVariable("name") name: String,
//                       @PathVariable("surname") surname:String,
//                       @PathVariable("age") age: Int): ResponseEntity<Any>
//    {
//        val person = Person(name, surname, age)
//        return ResponseEntity.ok(person)
//    }
//
//   @PostMapping("/echo")
//   fun echo(@RequestBody person: Person): ResponseEntity<Any>{
//       return ResponseEntity.ok(person)
//   }
//}