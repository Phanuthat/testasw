package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageShoppingCartDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getCustomer(): ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }

    @GetMapping("/shoppingcartpage")
    fun getShoppingCartWithPage(@RequestParam("page") page: Int,
                                @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartWithPage(page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @GetMapping("/shoppingcart/{productName}")
    fun getShoppingCartByProductName(@RequestParam("productName") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(
                shoppingCartService.getShoppingCartByProductName(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/shoppingcartpage/{productName}")
    fun getShoppingCartByProductWithPage(@RequestParam("productName") name: String,
                                         @RequestParam("page") page: Int,
                                         @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartByProductNameWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

//    @PostMapping("/shoppingcarts")
//    fun addShoppingCartCustomerSelectedProduct(@RequestBody("shoppingcart") a)
}

