package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartByProductName(name:String):List<ShoppingCart>
    fun getShoppingCartByProductNameWithPage(name: String,page:Int,pageSize:Int):Page<ShoppingCart>
    fun getCustomerByProduct(name: String):List<ShoppingCart>
    fun getShoppingCartWithPage(page:Int,pageSize:Int): Page<ShoppingCart>

}