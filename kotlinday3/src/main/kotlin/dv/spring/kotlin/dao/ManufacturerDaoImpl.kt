package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ManufacturerDaoImpl : ManufacturerDao {
    override fun findById(id: Long): Manufacturer? {
        return manufacturerRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    override fun save(manufacturer: Manufacturer): Manufacturer {
        return  manufacturerRepository.save(manufacturer)
    }

    override fun getManufacturers(): List<Manufacturer> {
        return mutableListOf(Manufacturer("Apple", "053123456"),
                Manufacturer("Samsung", "555666777888"),
                Manufacturer("CAMT", "0000000"))
    }

}